import pytest


def is_iterable_truthy_exists():
    from questions import is_iterable_truthy


def test_all_truthy_elements():
    from questions import is_iterable_truthy
    assert is_iterable_truthy([True, 1, "Hello, World!"]) == True


def test_one_falsy_element():
    from questions import is_iterable_truthy
    assert is_iterable_truthy([True, False, "Hello, World!"]) == False


def test_all_falsy_elements():
    from questions import is_iterable_truthy
    assert is_iterable_truthy([0, None, False, "", [], {}]) == False
