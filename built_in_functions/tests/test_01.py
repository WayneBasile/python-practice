import pytest


def test_no_negative_exists():
    from questions import no_negative


def test_no_negative_positive():
    from questions import no_negative
    assert no_negative(5) == 5


def test_no_negative_negative():
    from questions import no_negative
    assert no_negative(-3) == 3


def test_no_negative_zero():
    from questions import no_negative
    assert no_negative(0) == 0


def test_no_negative_large_positive():
    from questions import no_negative
    assert no_negative(10**10) == 10**10


def test_no_negative_large_negative():
    from questions import no_negative
    assert no_negative(-5**5) == 5**5


def test_no_negative_non_numeric():
    from questions import no_negative
    with pytest.raises(TypeError):
        no_negative("not a number")


def test_no_negative_none_input():
    from questions import no_negative
    with pytest.raises(TypeError):
        no_negative(None)
