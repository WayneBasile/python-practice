# QUESTION 1: Implement a Python function, no_negative, that takes a numeric value as input and returns its positive value.
# Write your answer below this line and run python -m pytest tests/test_01.py


# QUESTION 2: Implement a Python function, is_iterable_truthy, that takes an iterable as input and returns True if all elements are truthy and false otherwise.
# Write your answer below this line and run python -m pytest tests/test_02.py
